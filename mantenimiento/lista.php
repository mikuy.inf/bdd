<?php
include '../header.php';
include '../conexion.php'; 
$myArray=[];
$sql = "SELECT M.id,DATE_FORMAT(M.fecha, '%Y-%m-%d %H:%i:%s') as fecha, M.tabla, M.proceso, (SELECT CONCAT(nombre,' ',apellido) FROM usuarios WHERE id=M.usuario) as usuario FROM mantenimiento M ORDER BY M.id DESC";
if ($result = $mysqli->query($sql)){
  while($row = $result->fetch_assoc()) {
    $myArray[] = $row;
  }
  echo json_encode($myArray);
}
else{
  echo json_encode(array('mens'=>'No hay datos'));
}
$mysqli->close();
?>