<?php
include '../header.php';
include '../conexion.php'; 
$myArray=[];
$sql = "SELECT P.*, (SELECT nombre FROM categoria WHERE id=P.categoria) as nombrecategoria, (SELECT valor FROM iva WHERE id=P.iva) as valorIva,(IFNULL((SELECT SUM(valor) FROM stock WHERE producto=P.id AND (proceso=0 OR proceso=2) ),0)-IFNULL((SELECT SUM(valor) FROM stock WHERE producto=P.id AND proceso=1),0)) as cantidad FROM producto P";
if ($result = $mysqli->query($sql)){
  while($row = $result->fetch_assoc()) {
    $myArray[] = $row;
  }
  echo json_encode($myArray);
}
else{
  echo json_encode(array('mens'=>'No hay datos'));
}
$mysqli->close();
?>