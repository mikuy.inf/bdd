<?php
include '../header.php';
include '../conexion.php'; 
$myArray=[];
$sql = "SELECT f.*, (SELECT nombre FROM roles WHERE id=f.rol) as nombreRol FROM funciones f";
if ($result = $mysqli->query($sql)){
  while($row = $result->fetch_assoc()) {
      $row['crear'] = intval($row['crear']);
      $row['actualizar'] = intval($row['actualizar']);
      $row['leer'] = intval($row['leer']);
      $row['eliminar'] = intval($row['eliminar']);
    $myArray[] = $row;
  }
  echo json_encode($myArray);
}
else{
  echo json_encode(array('mens'=>'No hay datos'));
}
$mysqli->close();
?>