<?php
    include '../header.php';
    include '../conexion.php'; 
    $param = json_decode(file_get_contents("php://input"));
    
    $nombre=$param->nombre;
    $myArray=[];
    
    $sql = "SELECT f.*, (SELECT nombre FROM roles WHERE id=f.rol) as nombreRol FROM funciones f WHERE f.tabla LIKE '%$nombre%'";
    
    if ($result = $mysqli->query($sql)){
    while($row = $result->fetch_assoc()) {
        $row['crear'] = intval($row['crear']);
      $row['actualizar'] = intval($row['actualizar']);
      $row['leer'] = intval($row['leer']);
      $row['eliminar'] = intval($row['eliminar']);
        $myArray[] = $row;
    }
    echo json_encode($myArray);
    }
    else{
    echo json_encode(array('mens'=>'No hay datos'));
    }
    $mysqli->close();
?>