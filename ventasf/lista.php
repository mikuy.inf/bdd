<?php
include '../header.php';
include '../conexion.php'; 
$myArray=[];
$sql = "SELECT f.id,f.cedula,f.pasaporte,f.telefono, (SELECT CONCAT(nombre,' ',apellido) FROM usuarios WHERE id =f.usuario) as cliente,f.usuario, f.estado, f.subtotal,(SELECT SUM((ROUND(V.precio*V.cantidad,2))+ROUND(((((ROUND(V.precio*V.cantidad,2))*V.iva)/100.00)),2)-ROUND(((((ROUND(V.precio*V.cantidad,2))*V.descuento)/100.00)),2)) as valor FROM ventasd V WHERE V.ventasf =f.id) as total, DATE_FORMAT(f.fpago, '%Y-%m-%d %H:%i:%s') as fpago, f.iva FROM ventasf f; ";
if ($result = $mysqli->query($sql)){
  while($row = $result->fetch_assoc()) {
    $myArray[] = $row;
  }
  echo json_encode($myArray);
}
else{
  echo json_encode(array('mens'=>'No hay datos'));
}
$mysqli->close();
?>